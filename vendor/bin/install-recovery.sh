#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:83886080:72df23ecc4700708b8fda78d2b61e23b81e8399e; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:6ca57f8e737870867895034118e60758f64f25f3 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:83886080:72df23ecc4700708b8fda78d2b61e23b81e8399e && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
